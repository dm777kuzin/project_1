from django.db import models


class News(models.Model):
    header = models.CharField("Заголовок", max_length=255)
    content = models.TextField("Какой-то контент")
    photo = models.ImageField()
    create_date = models.DateTimeField("Дата создания")

    def __str__(self):
        return self.header

    class Meta:
        verbose_name = 'Новость'
        verbose_name_plural = 'Новости'


class Slider(models.Model):
    elem_name = models.CharField("Название элемента", max_length=255)

    def __str__(self):
        return self.elem_name

    class Meta:
        verbose_name = 'Слайдер'
        verbose_name_plural = 'Слайдеры'


class SliderElem(models.Model):
    slider = models.ForeignKey('Slider', on_delete=models.CASCADE)
    photo = models.ImageField()

    class Meta:
        verbose_name = 'Элемент слайдера'
        verbose_name_plural = 'Элементы слайдера'