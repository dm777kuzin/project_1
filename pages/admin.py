from django.contrib import admin
from .models import News, Slider, SliderElem

admin.site.register(News)
admin.site.register(Slider)
admin.site.register(SliderElem)
