# Generated by Django 3.0.8 on 2020-07-08 09:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='News',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('header', models.CharField(max_length=255, verbose_name='Заголовок')),
                ('content', models.TextField(verbose_name='Какой-то контент')),
                ('photo', models.ImageField(upload_to='')),
                ('create_date', models.DateTimeField(verbose_name='Дата создания')),
            ],
        ),
        migrations.CreateModel(
            name='Slider',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('elem_name', models.CharField(max_length=255, verbose_name='Название элемента')),
            ],
        ),
        migrations.CreateModel(
            name='SliderElem',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(upload_to='')),
                ('slider', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='pages.Slider')),
            ],
        ),
    ]
